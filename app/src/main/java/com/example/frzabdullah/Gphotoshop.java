package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Gphotoshop extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Adobe Photoshop");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gphotoshop);
    }

    public void bid(View view) {
        Intent a=new Intent(Gphotoshop.this,GPbasic.class);
        startActivity(a);
    }

    public void bid1(View view) {
        Intent a=new Intent(Gphotoshop.this,GPbeginner.class);
        startActivity(a);
    }

    public void bid2(View view) {
        Intent a=new Intent(Gphotoshop.this,GPadvenc.class);
        startActivity(a);
    }

    public void bid3(View view) {
        Intent a=new Intent(Gphotoshop.this,GPtips.class);
        startActivity(a);
    }

    public void bid4(View view) {
        Intent a=new Intent(Gphotoshop.this,GPappsDownload.class);
        startActivity(a);
    }
}
