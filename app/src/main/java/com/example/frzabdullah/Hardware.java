package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

public class Hardware extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Basic Hardware");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_hardware);

    }



    public void cpuid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_cpu.htm");
        startActivity(a);
    }

    public void inputid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_input_devices.htm");
        startActivity(a);
    }

    public void outputid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_output_devices.htm");
        startActivity(a);
    }

    public void memoryid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_memory.htm");
        startActivity(a);
    }

    public void ramid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_memory.htm");
        startActivity(a);
    }

    public void romid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_rom.htm");
        startActivity(a);
    }

    public void generationsid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_generations.htm");
        startActivity(a);
    }

    public void matherboarid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_motherboard.htm");
        startActivity(a);
    }

    public void memoryunitid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_memory_units.htm");
        startActivity(a);
    }

    public void portsid(View view) {
        Intent a=new Intent(Hardware.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_ports.htm");
        startActivity(a);
    }
}
