package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GxdBeginner extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Beginner Tutorial");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gxd_beginner);
    }

    public void btr(View view) {
        Intent a=new Intent(GxdBeginner.this,webView.class);
        a.putExtra("url","https://www.youtube.com/watch?v=lOyQ_Conh6k&list=PLi3UiuzFLacW5SDxgABAWg_F-0e-EiqOW");
        startActivity(a);
    }
}
