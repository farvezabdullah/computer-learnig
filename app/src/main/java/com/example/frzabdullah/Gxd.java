package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Gxd extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Adobe XD");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gxd);
    }

    public void xdid(View view) {
        Intent a=new Intent(Gxd.this,GxdBasic.class);
        startActivity(a);
    }

    public void xdid1(View view) {
        Intent a=new Intent(Gxd.this,GxdBeginner.class);
        startActivity(a);
    }

    public void xdid3(View view) {
        Intent a=new Intent(Gxd.this, GxdTips.class);
        startActivity(a);
    }

    public void xdid4(View view) {
        Intent a=new Intent(Gxd.this,Gxdsoftware.class);
        startActivity(a);
    }
}
