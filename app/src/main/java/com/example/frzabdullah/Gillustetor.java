package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Gillustetor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Adobe Illustrator");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gillustetor);
    }

    public void gid(View view) {
        Intent a=new Intent(Gillustetor.this,giBasic.class);
        startActivity(a);
    }

    public void gid1(View view) {
        Intent a=new Intent(Gillustetor.this,giBeginner.class);
        startActivity(a);
    }

    public void gid2(View view) {
        Intent a=new Intent(Gillustetor.this,giAdvenc.class);
        startActivity(a);
    }

    public void gid3(View view) {
        Intent a=new Intent(Gillustetor.this,giTips.class);
        startActivity(a);
    }

    public void gid4(View view) {
        Intent a=new Intent(Gillustetor.this,giAppsDownload.class);
        startActivity(a);
    }
}
