package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GPbeginner extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Beginner Tutorial");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gpbeginner);
    }

    public void btr(View view) {
        Intent a=new Intent(GPbeginner.this,webView.class);
        a.putExtra("url","https://www.youtube.com/watch?v=-cyYH6qGNF8&list=PLze-CR7sxGlSyY_AQzGxUoikzWS9YJhx5&index=1");
        startActivity(a);

    }
}
