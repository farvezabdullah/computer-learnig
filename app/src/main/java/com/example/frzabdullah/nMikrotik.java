package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class nMikrotik extends AppCompatActivity {
    private ListView listView;
    private String[] ccna;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Mikrotik");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_n_mikrotik);
        listView=(ListView)findViewById(R.id.list1);
        ccna=getResources().getStringArray(R.array.Mikrotik_Name);
        CustomAdapter adapter=new CustomAdapter(this,ccna);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String vale=ccna[position];
                if (position==0){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik-bangla-lecture-01-মাইক্রোটিক-কনফিগারে/");
                    startActivity(a);
                }
                if (position==1){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik-bangla-lecture-02-গেইটওয়ে-হিসেবে-মাইক্/");
                    startActivity(a);
                }
                if (position==2){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/186/");
                    startActivity(a);
                }
                if (position==3){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik-bangla-lecture-04-dhcp-সাভার্র-কনফিগার-করা/");
                    startActivity(a);
                }
                if (position==4){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik/mikrotik-bangla-lecture-05-bandwidth-control-by-mikrotik/");
                    startActivity(a);
                }
                if (position==5){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik/mikrotik-bangla-lecture-07mikrotik-রাউটার-backup-এবং-restore-করার-পদ/");
                    startActivity(a);
                }
                if (position==6){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik-bangla-lecture-08mikrotik-দ্বারা-hotspot-সেটআপ-করার-প/");
                    startActivity(a);
                }
                if (position==7){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik/mikrotik-bangla-lecture-09-mikrotik-এ-login-user-তৈরি-করার-পদ্ধতি/");
                    startActivity(a);
                }
                if (position==8){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik-bangla-lecture-10-mikrotik-router-এর-পাসওয়ার্ড-রিকভা/");
                    startActivity(a);
                }
                if (position==9){
                    Toast.makeText(nMikrotik.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nMikrotik.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/mikrotik/mikrotik-bangla-lecture-11-mikrotik-router-এ-টানেলিং-কনফিগার-ক/");
                    startActivity(a);
                }

            }
        });
    }
}
