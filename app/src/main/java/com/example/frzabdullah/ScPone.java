package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ScPone extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_sc_pone);

    }

    public void cbut1(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/how-to-install-c");
        startActivity(a);
    }

    public void cbut2(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/first-c-program");
        startActivity(a);
    }

    public void cbut3(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/flow-of-c-program");
        startActivity(a);
    }

    public void cbut4(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/printf-scanf");
        startActivity(a);
    }

    public void cbut5(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/variables-in-c");
        startActivity(a);
    }

    public void cbut6(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/data-types-in-c");
        startActivity(a);
    }

    public void cbut7(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/keywords-in-c");
        startActivity(a);
    }

    public void cbut8(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/c-operators");
        startActivity(a);
    }

    public void cbut9(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/comments-in-c");
        startActivity(a);
    }

    public void cbut10(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/escape-sequence-in-c");
        startActivity(a);
    }

    public void cbut11(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/constants-in-c");
        startActivity(a);
    }

    public void cbut12(View view) {
        Intent a=new Intent(ScPone.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/test/c-fundamental-1");
        startActivity(a);
    }
}
