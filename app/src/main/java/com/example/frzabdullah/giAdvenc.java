package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class giAdvenc extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Advance Tutorial");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gi_advenc);
    }

    public void btc(View view) {
        Intent a= new Intent(giAdvenc.this,webView.class);
        a.putExtra("url","https://www.youtube.com/watch?v=eSfvkZDzX9w&list=PLT1vcWz6l4EScJCOAM8_-8pdo1AmteFOE");
        startActivity(a);
    }
}
