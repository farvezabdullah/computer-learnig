package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ScPtwo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_sc_ptwo);
    }

    public void cbutt1(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/c-if-else");
        startActivity(a);
    }

    public void cbutt2(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/c-switch");
        startActivity(a);
    }

    public void cbutt3(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/c-loop");
        startActivity(a);
    }
    public void cbutt4(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/do-while-loop-in-c");
        startActivity(a);
    }

    public void cbutt5(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/while-loop-in-c");
        startActivity(a);
    }

    public void cbutt6(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/for-loop-in-c");
        startActivity(a);
    }

    public void cbutt7(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/c-break");
        startActivity(a);
    }

    public void cbutt8(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/c-continue");
        startActivity(a);
    }

    public void cbutt9(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/c-goto");
        startActivity(a);
    }

    public void cbutt10(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/type-casting-in-c");
        startActivity(a);
    }

    public void cbutt11(View view) {
        Intent a=new Intent(ScPtwo.this,webView.class);
        a.putExtra("url","https://www.javatpoint.com/test/c-control-statements-1");
        startActivity(a);
    }


}
