package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Software extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Programming Language");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_software);
    }

    public void cid(View view) {
        Intent a=new Intent(Software.this,ScProgram.class);
        startActivity(a);
    }

    public void ccid(View view) {
        Intent a= new Intent(Software.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/cplusplus/index.htm");
        startActivity(a);
    }

    public void cccid(View view) {
        Intent a= new Intent(Software.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/csharp/");
        startActivity(a);
    }

    public void javaid(View view) {
        Intent a= new Intent(Software.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/java/index.htm");
        startActivity(a);
    }

    public void androidid(View view) {
        Intent a= new Intent(Software.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/android/index.htm");
        startActivity(a);
    }

    public void pythonid(View view) {
        Intent a= new Intent(Software.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/python/index.htm");
        startActivity(a);
    }

    public void swiftid(View view) {
        Intent a= new Intent(Software.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/swift/index.htm");
        startActivity(a);
    }

    public void juliaid(View view) {
        Intent a= new Intent(Software.this,webView.class);
        a.putExtra("url","https://docs.julialang.org/en/v1/");
        startActivity(a);
    }

    public void kotlinid(View view) {
        Intent a= new Intent(Software.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/kotlin/index.htm");
        startActivity(a);
    }
}
