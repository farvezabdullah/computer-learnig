package com.example.frzabdullah;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
   DrawerLayout drawer;
   ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawer=(DrawerLayout)findViewById(R.id.drawerid);


        navigationView=findViewById(R.id.navigationid);
        navigationView.setNavigationItemSelectedListener(this);


        toggle=new ActionBarDrawerToggle(this,drawer,R.string.navi_open,R.string.navi_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item))
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void bb(View view) {
        Intent zzz = new Intent(MainActivity.this, Graphics.class);
        startActivity(zzz);
    }

    public void bb1(View view) {
        Intent zzz = new Intent(MainActivity.this, Hardware.class);
        startActivity(zzz);
    }

    public void bb2(View view) {
        Intent zzz = new Intent(MainActivity.this, Networking.class);
        startActivity(zzz);
    }

    public void bb3(View view) {
        Intent zzz = new Intent(MainActivity.this, WebDevelopment.class);
        startActivity(zzz);
    }

    public void bb4(View view) {
        Intent zzz = new Intent(MainActivity.this, Software.class);
        startActivity(zzz);
    }

    public void introductionid(View view) {
        Intent a=new Intent(MainActivity.this,Introduction.class);
        startActivity(a);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent a;
        if (item.getItemId()==R.id.aboutmenuId){
            a=new Intent(this,MainActivity.class);
            startActivity(a);
        }else if (item.getItemId()==R.id.feedmenuId){
            a=new Intent(this,Feedback_Activity.class);
            startActivity(a);
        }
        return false;
    }

    /*@Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id=menuItem.getItemId();

        if (id==R.id.aboutmenuId){
        setTitle("About");
        FragmentAbout fragmentAbout=new FragmentAbout();
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutId,fragmentAbout,"FragmentAbout");
        fragmentTransaction.commit();}

        else if (id==R.id.feedmenuId){
            setTitle("About");
            FragmentFeedback fragmentFeedback=new FragmentFeedback();
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameLayoutId,fragmentFeedback,"FragmentAbout");
            fragmentTransaction.commit();}

        return false;
    }*/
}
