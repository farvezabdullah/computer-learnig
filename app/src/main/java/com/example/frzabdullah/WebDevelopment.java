package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class WebDevelopment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Web Development");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_web_development);
    }

    public void htmlid(View view) {
        Intent a=new Intent(WebDevelopment.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/html/index.htm");
        startActivity(a);
    }

    public void cssid(View view) {
        Intent a=new Intent(WebDevelopment.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/css/index.htm");
        startActivity(a);
    }

    public void javasid(View view) {
        Intent a=new Intent(WebDevelopment.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/javascript/index.htm");
        startActivity(a);
    }

    public void phpid(View view) {
        Intent a=new Intent(WebDevelopment.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/php/index.htm");
        startActivity(a);
    }

    public void sqlid(View view) {
        Intent a=new Intent(WebDevelopment.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/mysql/index.htm");
        startActivity(a);
    }

    public void wordid(View view) {
        Intent a=new Intent(WebDevelopment.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/wordpress/index.htm");
        startActivity(a);
    }
}
