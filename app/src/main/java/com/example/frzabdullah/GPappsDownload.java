package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GPappsDownload extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Apps Download");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gpapps_download);
    }

    public void kaka(View view) {
        Intent a=new Intent(GPappsDownload.this,webView.class);
        a.putExtra("url","https://adobe-photoshop-cs6-update.en.softonic.com");
        startActivity(a);
    }
}
