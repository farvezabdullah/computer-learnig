package com.example.frzabdullah;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

public class Splash_Screen_Test extends AppCompatActivity {
    //ProgressBar work
    private ProgressBar progressBar;
    int progras;

    //spash scren
private static int SPASH_TIME_OUT=5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen__test);
        //ProgressBar work
        progressBar=findViewById(R.id.progressId);
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                doWork();
            }
        });
        thread.start();


        //spash scren
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent a=new Intent(Splash_Screen_Test.this,MainActivity.class);
                startActivity(a);
                finish();
            }
        },SPASH_TIME_OUT);
    }

    //ProgressBar work
    private void doWork() {
        for (progras=20;progras<=100;progras=progras+20){
            try {
                Thread.sleep(1000);
                progressBar.setProgress(progras);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
