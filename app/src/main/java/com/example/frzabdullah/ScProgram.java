package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class ScProgram extends AppCompatActivity {
ListView listView;
private String[] cprog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sc_program);
        listView=(ListView)findViewById(R.id.listcpro);
        cprog=getResources().getStringArray(R.array.Cprogram);
        CustomAdapter adapter=new CustomAdapter(this,cprog);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String vale=cprog[position];
                if (position==0){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,ScPone.class);
                    startActivity(a);
                }
                if (position==1){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,ScPtwo.class);
                    startActivity(a);
                }
                if (position==2){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_functions.htm");
                    startActivity(a);
                }
                if (position==3){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_arrays.htm");
                    startActivity(a);
                }
                if (position==4){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_pointers.htm");
                    startActivity(a);
                }
                if (position==5){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_memory_management.htm");
                    startActivity(a);
                }
                if (position==6){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_strings.htm");
                    startActivity(a);
                }
                if (position==7){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.programiz.com/c-programming/library-function/math.h");
                    startActivity(a);
                }
                if (position==8){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_unions.htm");
                    startActivity(a);
                }
                if (position==9){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_error_handling.htm");
                    startActivity(a);
                }
                if (position==10){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_preprocessors.htm");
                    startActivity(a);
                }
                if (position==11){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.tutorialspoint.com/cprogramming/c_command_line_arguments.htm");
                    startActivity(a);
                }
                if (position==12){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.javatpoint.com/test/c-programming");
                    startActivity(a);
                }
                if (position==13){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.javatpoint.com/c-programs");
                    startActivity(a);
                }
                if (position==14){
                    Toast.makeText(ScProgram.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(ScProgram.this,webView.class);
                    a.putExtra("url","https://www.javatpoint.com/c-interview-questions");
                    startActivity(a);
                }
            }
        });
    }
}
