package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class nCCNA extends AppCompatActivity {
private ListView listView;
private String[] ccna;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("CCNA R & S");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_n_ccn);
        listView=(ListView) findViewById(R.id.list);
        ccna=getResources().getStringArray(R.array.CCNA_Name);
        CustomAdapter adapter=new CustomAdapter(this,ccna);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String vale=ccna[position];

                //Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                if (position==0){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-02-বেসিক-নেটওয়ার্কিং/");
                    startActivity(a);
                }
                if (position==1){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-০২-ওএসআই-মডেল/");
                    startActivity(a);
                }
                if (position==2){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/others/ccna-bangla-০৩-টিসিপিআইপি/");
                    startActivity(a);
                }
                if (position==3){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-০৪-ক্লাস-সি-সাবনেটিং/");
                    startActivity(a);
                }
                if (position==4){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-০৫-ক্লাস-বি-সাবনেটিং/");
                    startActivity(a);
                }
                if (position==5){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-০৬-ক্লাস-এ-সাবনেটিং/");
                    startActivity(a);
                }
                if (position==6){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-০৭-vlsm/");
                    startActivity(a);
                }
                if (position==7){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-লেকচার-৮-বেসিক-রাউটিং/");
                    startActivity(a);
                }
                if (position==8){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-লেকচার-৯-স্ট্যাটিক-রা/");
                    startActivity(a);
                }
                if (position==9){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-লেকচার-১০-ডায়নামিক-রাউ/");
                    startActivity(a);
                }
                if (position==10){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-লেকচার-১১-ডায়নামিক-রা/");
                    startActivity(a);
                }
                if (position==11){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-লেকচার-১২-সুইচিং-বেসিক/");
                    startActivity(a);
                }
                if (position==12){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-লেকচার-১৩-একসেস-কন্ট্র/");
                    startActivity(a);
                }
                if (position==13){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-পরিচিতি-লেকচার-১৪-nat/");
                    startActivity(a);
                }
                if (position==14){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna-bangla-লেকচার-১৫-ipv6/");
                    startActivity(a);
                }
                if (position==15){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna/ccna-bangla-লেকচার-১৬-wan/");
                    startActivity(a);
                }
                if (position==16){
                    Toast.makeText(nCCNA.this,vale,Toast.LENGTH_SHORT).show();
                    Intent a=new Intent(nCCNA.this,webView.class);
                    a.putExtra("url","http://www.tsoftit.com/tutorial/ccna/ccna-bangla-লেকচার-১৭-hsrpvrrp-glbp/");
                    startActivity(a);
                }


            }
                    });


                    }


                    }
