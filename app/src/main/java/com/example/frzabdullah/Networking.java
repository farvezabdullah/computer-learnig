package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Networking extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setTitle("Networking");
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_networking);
    }

    public void gid(View view) {
        Intent a=new Intent(Networking.this,nCCNA.class);
        startActivity(a);
    }

    public void gid1(View view) {
        Intent b=new Intent(Networking.this,nMikrotik.class);
        startActivity(b);
    }

    public void basicid(View view) {
        Intent a=new Intent(Networking.this,webView.class);
        a.putExtra("url","https://www.tutorialspoint.com/computer_fundamentals/computer_networking.htm");
        startActivity(a);
    }
}
