package com.example.frzabdullah;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter
{
    String[] ccna;
    Context context;
    private LayoutInflater inflater;

    CustomAdapter (Context context,String[]ccna){
        this.context=context;
        this.ccna=ccna;
    }
    @Override
    public int getCount() {
        return ccna.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){

           inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           convertView=inflater.inflate(R.layout.activity_sample_view,parent,false);


        }
        TextView textView=(TextView)convertView.findViewById(R.id.teccna);
        textView.setText(ccna[position]);

        return convertView;
    }
}
