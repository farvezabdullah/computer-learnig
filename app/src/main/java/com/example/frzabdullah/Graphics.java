package com.example.frzabdullah;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Graphics extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Graphics Design");
        //Back Buttom Work
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_graphics);
    }

    public void gb(View view) {
        Intent a=new Intent(Graphics.this,Gphotoshop.class);
        startActivity(a);
    }

    public void gi(View view) {
        Intent b=new Intent(Graphics.this,Gillustetor.class);
        startActivity(b);
    }

    public void gxd(View view) {
        Intent c=new Intent(Graphics.this,Gxd.class);
        startActivity(c);
    }
}
