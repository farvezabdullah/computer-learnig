package com.example.frzabdullah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class giBeginner extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("Beginner Tutorial");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gi_beginner);
    }

    public void btr(View view) {
        Intent a=new Intent(giBeginner.this,webView.class);
        a.putExtra("url","https://www.youtube.com/watch?v=REKyfIRJklU&list=PLsv4iUgyA4ipZO5dE0bOh7ShkGSrZ511a");
        startActivity(a);
    }
}
